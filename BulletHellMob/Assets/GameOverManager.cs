using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverManager : MonoBehaviour
{
    private void Start()
    {
        GestorDeAudio.instancia.ReproducirSonido("GameOver");
    }

}
