using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPlayer : MonoBehaviour
{
    public int damage = 20;
    public float speed = 10f;
    [SerializeField] Transform target;

    public void SetTarget(Transform enemyTransform)
    {
        target = enemyTransform;
    }


    private void Start()
    {
        PlayerRotation player = GameObject.FindObjectOfType<PlayerRotation>();

        SetTarget(player.GetTarget());

    }

    private void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector2 direction = (target.position - transform.position).normalized;
        transform.Translate(direction * speed * Time.deltaTime, Space.World);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Detectar colisi�n con un enemigo y causarle da�o
        if (collision.CompareTag("Enemy"))
        {
            Enemy enemy = collision.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.TakeDamage(damage);
                Debug.Log("Le estoy haciendo da�o al enemigo");
            }

            Destroy(gameObject); // Destruir la bala al impactar con un enemigo
        }
    }
}
