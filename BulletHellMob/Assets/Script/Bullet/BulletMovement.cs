using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    public float speed = 10f; 
    public int damage = 10; 
    public Sprite[] bulletSprites; // Array de sprites para el proyectil
    public float lifetime = 3f;

    private Vector3 direction; 

    private void Start()
    {
        // Seleccionar aleatoriamente un sprite para el proyectil
        if (bulletSprites.Length > 0)
        {
            int randomIndex = Random.Range(0, bulletSprites.Length);
            SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = bulletSprites[randomIndex];
        }
    }

    private void Update()
    {
        transform.position += direction * speed * Time.deltaTime;
    }

    public void SetDirection(Vector3 dir)
    {
        direction = dir.normalized; // Normalizo la direcci�n para asegurarnos de que el proyectil se mueva a una velocidad constante
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // Comprobar si el proyectil colisiona con el jugador
        if (other.CompareTag("Player") || !other.isTrigger)
        {
            Debug.Log("Le llego el disparo");
            Player player = other.GetComponent<Player>(); 
            player.TakeDamage(damage); 
            Destroy(gameObject, lifetime); // Destruir el proyectil despu�s de impactar
        }
    }
}
