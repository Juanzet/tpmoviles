using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerShooting : MonoBehaviour
{
    public Transform shootingPoint;
    public GameObject bulletPrefab;
    public float fireRate = 0.5f;
    private float nextFireTime;
    public int damage = 30;
    

    Enemy[] enemies;

    public Image Skill1, Skill2, Skill3;
    public Image cdImage;

    [SerializeField] HabilidadLluvia acidRainSkill;

    bool canUseSkill = true;

    float lastTapTime = 0;
    float doubleTapThreshold = 0.3f;
    float dragDistance;

    Vector3 firstPos, lastPos;

    private void Start()
    {
        nextFireTime = 0f;
        dragDistance = Screen.height * 15 / 100;
    }

    private void Update()
    {
        // Disparar si se toca la pantalla y ha pasado el tiempo de espera
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                firstPos = touch.position;
                lastPos = touch.position;

                if (Time.time - lastTapTime <= doubleTapThreshold && canUseSkill)
                {
                    lastTapTime = 0;
                    acidRainSkill.CreateRainZone(touch.position);
                    StartCoroutine(SkillCooldown(acidRainSkill.CooldownTime, Skill1));
                }
                else if (Time.time >= nextFireTime)
                {
                    lastTapTime = Time.time;
                    Shoot();
                    nextFireTime = Time.time + 1f / fireRate;
                }
            }
            else if (touch.phase == TouchPhase.Moved)
                lastPos = touch.position;
            else if(touch.phase == TouchPhase.Ended)
            {
                lastPos = touch.position;

                if(Mathf.Abs(lastPos.x - firstPos.x) > dragDistance || Mathf.Abs(lastPos.y - firstPos.x) > dragDistance)
                {
                    if(Mathf.Abs(lastPos.x - firstPos.x) > Mathf.Abs(lastPos.y - firstPos.y))
                    {
                        if(lastPos.x > firstPos.x)
                        {

                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        if(lastPos.y > firstPos.y)
                        {

                        }
                        else
                        {

                        }
                    }
                }

            }

           
        }
        
    }

    private void Shoot()
    {

        enemies = new Enemy[0]; // Clear the array by assigning an empty array
        enemies = FindObjectsOfType<Enemy>();
        if(enemies.Length > 0)
        {
            // Crear una nueva bala en el punto de origen del disparo
            GameObject bullet = Instantiate(bulletPrefab, shootingPoint.position, shootingPoint.rotation);

            // Aplicar velocidad a la bala
            bullet.GetComponent<Rigidbody2D>().velocity = transform.forward * 10f;
        }

    }

    IEnumerator SkillCooldown(float pTime, Image pImage)
    {

        pImage.gameObject.SetActive(true);

        cdImage.gameObject.SetActive(true);

        cdImage.transform.position = pImage.transform.position;

        float mTime = 0;

        while(mTime < pTime)
        {
            yield return new WaitForSecondsRealtime(1.0f);
            mTime++;
            cdImage.fillAmount = mTime/10;
        }

        canUseSkill = true;

        pImage.gameObject.SetActive(false);

        cdImage.gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Detectar colisi�n con un enemigo y causarle da�o
        if (collision.CompareTag("Enemy"))
        {
            Enemy enemy = collision.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.TakeDamage(damage);
            }
        }
    }
}
