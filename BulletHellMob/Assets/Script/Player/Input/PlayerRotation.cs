using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotation : MonoBehaviour
{
    Transform target;

    List<GameObject> enemysInDungeon = new List<GameObject>();


    public void GetEveryEnemy()
    {
        Transform[] objectsInScene = GameObject.FindObjectsOfType<Transform>();

        for (int i = 0; i < objectsInScene.Length; i++)
        {
            if (objectsInScene[i].gameObject.CompareTag("Enemy"))
                enemysInDungeon.Add(objectsInScene[i].gameObject);
        }

    }

    

    Transform CloserEnemy()
    {
        Transform closestEnemy = transform;
        float leastDistance = Mathf.Infinity;

        foreach (var v in enemysInDungeon)
        {
            if(v != null)
            {
                float distanceBetween = Vector3.Distance(this.transform.position, v.transform.position);

                if (distanceBetween < leastDistance)
                {
                    leastDistance = distanceBetween;
                    closestEnemy = v.transform;
                }
            }
            
        }

        return closestEnemy;
    }

    void TrackObjective()
    {
        
        target = CloserEnemy();


        float radianAngle = Mathf.Atan2(target.position.y - transform.position.y, target.position.x - transform.position.x);
        float degreeAngle = (180 / Mathf.PI) * radianAngle /*- 90*/;
        transform.rotation = Quaternion.Euler(0, 0, degreeAngle);



    }

    public Transform GetTarget()
    {
        return target;
    }

    private void Update()
    {
        foreach(var v in enemysInDungeon)
        {
            if (v is null)
                enemysInDungeon.Remove(v);
        }

        TrackObjective();
    }


}
