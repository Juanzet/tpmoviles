using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public int health = 100;
    public int damage = 30;

    public RectTransform[] HealthBars;
    private float[] initialWidths;

    private void Start()
    {
        initialWidths = new float[HealthBars.Length];
        for (int i = 0; i < HealthBars.Length; i++)
        {
            initialWidths[i] = HealthBars[i].sizeDelta.x;
        }
    }

    private void Update()
    {
        UpdateHealthBars();
    }

    public void TakeDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            Die();
        }

        UpdateHealthBars();
    }

    private void Die()
    {
        Debug.Log("El jugador muri�");
    }

    private void UpdateHealthBars()
    {
        float healthPercentage = (float)health / 100;
        for (int i = 0; i < HealthBars.Length; i++)
        {
            Vector2 size = HealthBars[i].sizeDelta;
            size.x = initialWidths[i] * healthPercentage;
            HealthBars[i].sizeDelta = size;
        }
    }
}