using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float speed, maxSpeed;

    Vector2 input;
    
    Rigidbody2D _rigidbody;

    

    void GatherInput()
    {
        input = new Vector2(Input.acceleration.x, Input.acceleration.y);
    }

    void MoveCharacter()
    {
        Vector2 currentVelocity = _rigidbody.velocity;
        Vector2 targetVelocity = input * speed;

        Vector2 velocityChange = targetVelocity - currentVelocity;
        velocityChange = Vector2.ClampMagnitude(velocityChange, maxSpeed);

        _rigidbody.AddForce(velocityChange, ForceMode2D.Impulse);

    }


    private void Start()
    {
        _rigidbody = this.GetComponent<Rigidbody2D>();
    }


    private void Update()
    {
        GatherInput();
    }



    private void FixedUpdate()
    {
        MoveCharacter();
    }

}
