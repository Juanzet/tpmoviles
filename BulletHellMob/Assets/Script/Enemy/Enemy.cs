using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public float speed = 5f;
    public int maxHealth = 100;
    private int currentHealth;

    public GameObject bulletPrefab;
    public Transform firePoint; // Punto de origen del disparo del enemigo
    public float fireRate = 2f;
    public Animator animator;

    private Transform player;
    private float nextFireTime;
    public Player _player;
    public RectTransform[] HealthBars;
    private float[] initialWidths;

    private bool isIdle = true;
    private bool playerDetected = false;
    private bool isPlayerDead = false;
    private bool canShoot = true;

    private void Start()
    {
        initialWidths = new float[HealthBars.Length];
        for (int i = 0; i < HealthBars.Length; i++)
        {
            initialWidths[i] = HealthBars[i].sizeDelta.x;
        }
        animator = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        player.gameObject.GetComponent<PlayerRotation>().GetEveryEnemy();
        _player = GameObject.FindObjectOfType<Player>();
        currentHealth = maxHealth;
        // Configuro el pr�ximo momento de disparo
        nextFireTime = Time.time + 1f / fireRate;
    }

    private void Update()
    {
        UpdateHealthBars();

        if (player != null)
        {
            // Moverse hacia el jugador si ha sido detectado
            if (playerDetected)
            {
                Vector3 direction = (player.position - transform.position).normalized;
                transform.position += direction * speed * Time.deltaTime;
            }

            // Disparar hacia el jugador si es posible
            if (Time.time >= nextFireTime && _player.health > 0)
            {
                if (!isPlayerDead)
                    Shoot();
                nextFireTime = Time.time + 1f / fireRate; // Actualiza el pr�ximo momento de disparo
            }


            if (!isPlayerDead && !playerDetected && isIdle)
            {
                // Cambiar al estado de idle
                animator.SetBool("isIdle", true);
            }
            else
            {
                // Restablecer el estado de idle
                animator.SetBool("isIdle", false);
            }
        }

        if (_player.health <= 0)
        {
            isPlayerDead = true;
            canShoot = false;
            // Detener el disparo y volver a la animaci�n de idle
            animator.SetBool("isIdle", true);
            animator.SetBool("isAttacking", false);
            // Hacer que el jugador desaparezca
            player.gameObject.SetActive(false);
            return;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            playerDetected = true;

            // Reproducir animaci�n de detecci�n
            animator.SetBool("playerDetected", playerDetected);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            playerDetected = false;
            animator.SetBool("playerDetected", playerDetected);
        }
    }

    private void Shoot()
    {       

        // Creo un nuevo proyectil en el punto de origen del disparo
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Vector3 direction = (player.position - transform.position).normalized;
        BulletMovement bulletMovement = bullet.GetComponent<BulletMovement>();
        bulletMovement.SetDirection(direction);
        // Animaci�n de disparo
        animator.SetBool("isIdle", false);
        animator.SetBool("isAttacking", true);
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;

        if (currentHealth <= 0)
        {
            Die();
        }

        UpdateHealthBars();
    }

    private void Die()
    {
        animator.SetBool("isIdle", false);
        animator.SetBool("isAttacking", false);
        animator.SetBool("isDead", true);
        Destroy(gameObject, 1f);
    }

    private void OnDestroy()
    {
        GameManager.instance.AddEnemyKilledToDungeon();
    }

    private void UpdateHealthBars()
    {
        float healthPercentage = (float)currentHealth / 100;
        for (int i = 0; i < HealthBars.Length; i++)
        {
            Vector2 size = HealthBars[i].sizeDelta;
            size.x = initialWidths[i] * healthPercentage;
            HealthBars[i].sizeDelta = size;
        }
    }
}
