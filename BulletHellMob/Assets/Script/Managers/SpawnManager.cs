using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] GameObject enemyPrefab;

    public void SpawnEnemy(Transform mSpawnPoint)
    {
        Instantiate(enemyPrefab, mSpawnPoint.position, mSpawnPoint.rotation);
    }

}
