using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomSwapper : MonoBehaviour
{
    [SerializeField] int roomIndex;

    [HideInInspector]
    public Animator anim;

    private void Awake()
    {
        anim = this.GetComponent<Animator>();
    }

    private void Start()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent<PlayerMovement>(out PlayerMovement p))
            GameManager.instance.ChangeCamFocus(roomIndex);
    }
}
