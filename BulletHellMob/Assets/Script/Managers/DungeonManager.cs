using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonManager : MonoBehaviour
{
    [SerializeField] GameObject[] roomSwappers;

    [SerializeField] GameObject[] enemySpawners;

    int enemysToSpawn = 2;

    int enemyKilled, enemyCount;

    public bool isDungeonAlive;

    private void Start()
    {
        InvokeRepeating("SpawnEnemy", 1.5f, 3.5f);
    }

    private void Update()
    {
        if (isDungeonAlive)
        {
            if (enemyKilled >= enemysToSpawn)
            {
                foreach (var v in roomSwappers)
                {
                    v.gameObject.GetComponent<RoomSwapper>().anim.SetTrigger("OpenDoor");
                    v.GetComponent<BoxCollider2D>().isTrigger = true;
                }
            }
        }

        
    }

    public void AddEnemyKilled()
    {
        enemyKilled += 1;
    }

    int previousSpawned = -1;
    void SpawnEnemy()
    {
        if(enemyCount < enemysToSpawn)
        {

            int mRandom = (int)Random.Range(0, 4);

            while(previousSpawned == mRandom)
            {
                mRandom = (int)Random.Range(0, 4);
            }

            enemySpawners[mRandom].GetComponent<SpawnManager>().SpawnEnemy(enemySpawners[mRandom].transform);

            enemyCount++;

            previousSpawned = mRandom;
        }
    }

    public void StartDungeon()
    {
        this.gameObject.SetActive(true);

        StartCoroutine(EnableSwappers(1.5f));

        foreach (var v in enemySpawners)
        {
            v.gameObject.SetActive(true);
        }

        enemysToSpawn = (int)Random.Range(1, 9);

        isDungeonAlive = true;
    }

    public void EndDungeon()
    {

        foreach (var v in enemySpawners)
        {
            v.gameObject.SetActive(false);
        }

        foreach (var v in roomSwappers)
        {
            v.gameObject.SetActive(false);
            v.GetComponent<SpriteRenderer>().enabled = true;
            v.GetComponent<BoxCollider2D>().isTrigger = false;
        }

        this.gameObject.SetActive(false);

        isDungeonAlive = false;
    }
    

    IEnumerator EnableSwappers(float pTime)
    {
        float mTime = pTime;

        for (int i = 0; i < roomSwappers.Length; i++)
        {
            roomSwappers[i].gameObject.SetActive(true);
            roomSwappers[i].GetComponent<RoomSwapper>().anim.SetTrigger("OpenDoor");

        }

        while (mTime > 0)
        {
            yield return new WaitForSecondsRealtime(1.0f);
            mTime--;
        }

        for(int i = 0; i < roomSwappers.Length; i++)
        {            
            roomSwappers[i].GetComponent<RoomSwapper>().anim.SetTrigger("CloseDoor");            
        }

    }
}
