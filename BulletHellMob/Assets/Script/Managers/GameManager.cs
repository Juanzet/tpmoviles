using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] Transform cam;

    [SerializeField] Transform[] dungeonPoints;

    [SerializeField] DungeonManager[] dungeons;

    [SerializeField] GameObject player;

    [SerializeField] Enemy[] enemysInScene;

    public static GameManager instance;

    [SerializeField] Image menuImage;

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        GestorDeAudio.instancia.ReproducirSonido("Level");
    }

    public void AddEnemyKilledToDungeon()
    {
        foreach(var v in dungeons)
        {
            if (v.isDungeonAlive)
                v.AddEnemyKilled();
        }
    }


    public void ChangeCamFocus(int pIndex)
    {
        cam.transform.position = new Vector3(dungeonPoints[pIndex].position.x, dungeonPoints[pIndex].position.y, -10);

        foreach(var v in dungeons)
        {
            if (v.isDungeonAlive)
            {
                v.EndDungeon();
            }
            
        }

        dungeons[pIndex].StartDungeon();
    }

    public void OpenPauseMenu()
    {
        player.GetComponent<Player>().enabled = false;
        player.GetComponent<PlayerShooting>().enabled = false;
        player.GetComponent<PlayerRotation>().enabled = false;
        player.GetComponent<PlayerMovement>().enabled = false;

        enemysInScene = GameObject.FindObjectsOfType<Enemy>();

        foreach (var v in enemysInScene)
        {           
            v.enabled = false;
        }

        LeanTween.scale(menuImage.gameObject, new Vector3(1, 1, 1), 1.5f);

    }


    public void ClosePauseMenu()
    {      
        LeanTween.scale(menuImage.gameObject, new Vector3(0, 0, 0), 1.5f);

        player.GetComponent<Player>().enabled = true;
        player.GetComponent<PlayerShooting>().enabled = true;
        player.GetComponent<PlayerRotation>().enabled = true;
        player.GetComponent<PlayerMovement>().enabled = true;

        foreach (var v in enemysInScene)
        {
            v.enabled = true;
        }

        enemysInScene = null;
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void CloseGame()
    {
        Application.Quit();
    }

}
