using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingRotation : MonoBehaviour
{
    [Header("MovimientoCamara")]
    Vector3 target;
    [SerializeField] Camera cam;

    [Header("MovimientoJugador")]
    [SerializeField] float moveSpeed;
    Vector2 direction;
    Rigidbody2D rb;
    Vector2 input;

    void Start() 
    {
        rb = GetComponent<Rigidbody2D>();    
    }

    void Update()
    {
        // en el region tenemos un script sencillo de trackeo de elementos, en este caso estamos usando el trackeo pero hacia la posicion del mouse
        // depende la situacion podemos utilizar otras formas de implementacion esta me parecio la mas optima
        // de todas formas hay que cambiarlo a la posicion del dedo en pantalla y no a la del mouse o puede que lo tomemos de otra forma 
        // lo dejo asi a modo place holder

        target = cam.ScreenToWorldPoint(Input.mousePosition);

        float radianAngle = Mathf.Atan2(target.y - transform.position.y, target.x - transform.position.x);
        float degreeAngle = (180 / Mathf.PI) * radianAngle - 90; 
        transform.rotation = Quaternion.Euler(0, 0, degreeAngle);

        #region script para trackear un objetivo
        //  le damos la posicion relativa de nuestro personaje objetivo para calcular el angulo apartir de esta posicion(donde se encuentra mirando)
        // le restamos 90 por el desface de la rotacion inicial
        // calculamos el angulo apartir de la posicion con quaternion euler
        //float radianAngle = Mathf.Atan2(target.position.y - transform.position.y, target.position.x - transform.position.x);
        //float degreeAngle = (180 / Mathf.PI) * radianAngle - 90; 
        //transform.rotation = Quaternion.Euler(0, 0, degreeAngle);
        #endregion

        input.x = Input.GetAxisRaw("Horizontal");
        input.y = Input.GetAxisRaw("Vertical");
        direction = input.normalized;
    }

    void FixedUpdate() 
    {
        rb.MovePosition(rb.position + direction * moveSpeed * Time.deltaTime);
    }
}
