using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoting : MonoBehaviour
{
    [SerializeField] Transform shotPoint;
    [SerializeField] GameObject bullet;

    void Update()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            Shot();
        }
    }

    void Shot()
    {
        Instantiate(bullet, shotPoint.position, shotPoint.rotation);
    }
}
